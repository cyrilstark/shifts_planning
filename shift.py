import pandas as pd
from ortools.linear_solver import pywraplp
from IPython import embed
import numpy as np


input_filename="./input.xlsx"
report_filename="./result2.csv"

df_nurses = pd.read_excel(input_filename,sheet_name="nurses")
df_shifts = pd.read_excel(input_filename,sheet_name="requirements")
df_shift_preferences = pd.read_excel(input_filename,sheet_name="shift_preferences")


########################
# setup master df
########################

# df_shifts['fake_key']=1
# df_nurses['fake_key']=1
# df = df_shifts.merge(df_nurses,on='fake_key')\
# 			  .drop('fake_key',axis=1)

# m = df['req_level']<=df['level']
# df = df[m]


# TODO: 
df_shift_nurses = df_shifts.merge(df_nurses,left_on='req_level',right_on='level',
					how='inner')



df_shift_nurses_pref = df_shift_nurses.merge(df_shift_preferences,
					on=['shift_id','name'],how='left')



solver = pywraplp.Solver('SolveIntegerProblem', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
params = pywraplp.MPSolverParameters()
params.SetDoubleParam(params.RELATIVE_MIP_GAP, 0.01)
# params.SetIntegerParam(params.N_THREADS, 10)


l = [ (name,shift_id,solver.IntVar(0.0,1.0,'x_{}_{}'.format(name,shift_id)))
		for name in df_nurses.name.unique() 
		for shift_id in df_shifts.shift_id.unique()]
names = [n for n,s,var in l]
shifts = [s for n,s,var in l]
variables = [var for n,s,var in l]



########################
# add opt vars
########################

df_x = pd.DataFrame({'name':names,'shift_id':shifts,'x':variables})


df = df_x.merge(df_shift_nurses_pref,on=['name','shift_id'],how="left")\
		[["shift_id","req_level","amount","name",
		"level","preference","x"]]


# # remove rows with impossible shift<->nurse combinations
# m = df['req_level']==df['level']
# df = df[m]

########################
# add constraints
########################

# obj fun
obj_fn_summands = [1,]


# shift requirements
m = pd.notna(df['req_level']) | pd.notna(df['level'])
assert np.all( df['req_level'][m]==df['level'][m] )

df_tmp = df.groupby(['shift_id','level'])\
		   .agg({'x':solver.Sum,
		   		 'amount': [min,max]})\
		   .rename(columns={'x':'sum(x)'})
assert np.all(df_tmp['amount', 'min']==df_tmp['amount', 'max'])

# map multiindex to index
df_tmp = df_tmp[[('sum(x)', 'Sum'),('amount', 'min')]]
df_tmp.columns = df_tmp.columns.get_level_values(0)


for index,row in df_tmp.iterrows():
	solver.Add(row['sum(x)']==row['amount'])


# # hard preference constraints
# print "** WARN: hard pref constraints likely lead to infeasibility"
# m = pd.notna(df['preference'])
# df_tmp = df[m]
# for index,row in df_tmp.iterrows():
# 	solver.Add(row['x']<=row['preference'])



###########
# solve
###########

obj_val = solver.NumVar(-solver.Infinity(), solver.Infinity(), "obj_val")
solver.Add(solver.Sum(obj_fn_summands) == obj_val)

objective = solver.Maximize(obj_val)


result_status = solver.Solve(params)
assert result_status==pywraplp.Solver.OPTIMAL
assert solver.VerifySolution(1e-7,True)


df_solution = pd.DataFrame(
		{'solution':[ e.solution_value() for e in df_x['x']],
		 'name':df_x['name'].tolist(),
		 'shift_id':df_x['shift_id'].tolist()})

m = df_solution['solution']==1.
df_solution = df_solution[m].sort_values('shift_id')
print df_solution

df = df.merge(df_solution,on=['name','shift_id'],how='outer')\
			.drop('x',axis=1)




# print solver.ExportModelAsLpFormat(False)
df.to_csv(report_filename,index=False)




